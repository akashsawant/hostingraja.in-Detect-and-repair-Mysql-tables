Shell Script To detect and repair mysql tables automatically on Vps Server of HostingRaja
=========================================================================================
MySQL database can be used to run a dynamic websites which will be running on the vps server.MYSQL an open source relational database management system when combined together with any server side programming language is capable of delivering highly unique solutions.If mysql databases get corrupted it will impact on website, the database corruption will be due to:


=> MySQL server daemon restarts suddenly,Either your server reboots suddenly, or you kill the mysqld process, or it hits an assertion and kills itself.

=> A database very frequently uses memory based buffers, caches, and log files along with the major ones, data files and index files. The database buffer cache is the place where the database server copies data from data files and place for processing. With a growing data volume on the database, the information placed on the file system will increase, and if a proportionate additional memory allocation is not done for the in-memory resources, the database will try to access the SWAP memory (memory allocated on the disk to be used by processes once RAM is being completely used up). If enough swap space is not available at any point in time, then the database server may abort the operation or abruptly crash due to lack of memory.

=> A significant percentage of database server crashes are caused by file permission issues, corrupted data, and index files. This can happen due to a variety of reasons:
   a)Some other processes are trying to modify a data or index file that is written by the database without suitable locking.
   b)The database server tries to read/write from an already crashed and thus corrupted data/index
   c)The data/file permission is either not set properly initially or was changed after the database deployment, preventing the database server from accessing the files.

=> A typical scenario of poor DBA performance is the database crash that happens during an upgrade, when the dependencies of the new release are not ensured prior to the upgrade. 

=> A bad query or recursive querying can hang the server or cause buffer flow that crashes the server.
This scripts is very helpful to check the mysql database status and if any defects found it will repair it automatically without any data loss.By this you will save your time in checking the databases manually and be free from tensions regarding the backup of database.

Once you donw with this restart the mysql service using the command: 

service mysqld restart

For more details visit us at hostingraja.in


